core = 7.x
api = 2

; addtoany
projects[addtoany][type] = "module"
projects[addtoany][download][type] = "git"
projects[addtoany][download][url] = "https://git.uwaterloo.ca/drupal-org/addtoany.git"
projects[addtoany][download][tag] = "7.x-4.0"
projects[addtoany][subdir] = ""

; uw_fdsu_theme_sotu
projects[uw_fdsu_theme_sotu][type] = "theme"
projects[uw_fdsu_theme_sotu][download][type] = "git"
projects[uw_fdsu_theme_sotu][download][url] = "https://git.uwaterloo.ca/msi/uw_fdsu_theme_sotu.git"
projects[uw_fdsu_theme_sotu][download][tag] = "7.x-1.14"
projects[uw_fdsu_theme_sotu][subdir] = ""

; uw_stories
projects[uw_stories][type] = "module"
projects[uw_stories][download][type] = "git"
projects[uw_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_stories.git"
projects[uw_stories][download][tag] = "7.x-1.2"
projects[uw_stories][subdir] = ""

; uw_ct_stories
projects[uw_ct_stories][type] = "module"
projects[uw_ct_stories][download][type] = "git"
projects[uw_ct_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_stories.git"
projects[uw_ct_stories][download][tag] = "7.x-2.8"
projects[uw_ct_stories][subdir] = ""

